#include<iostream>
#include<fstream>
using namespace std;

struct st{
	int val;
	st *left;
};
struct vr{
	string name;
	int val;
	vr *right;
};
struct comm{
	int line;
	string action;
	string name1;
	string name2;
	int arg1;
	int arg2;
	comm *left;
	comm *right;
};
vr *zm=NULL;
st *stack = NULL;

void put(int a){
	if(stack == NULL){
		stack = new st;
		stack->val = a;
		stack->left = NULL;
		return;
	}
	st *tmp = new st;
	tmp->val = a;
	tmp->left = stack;
	stack = tmp;
}

int pop(){
	int a = stack->val;
	stack = stack->left;
	return a;
}

void set(string a,int b){
	if(zm==NULL){
		zm=new vr;
		zm->val = b;
		zm->name = a;
		return;
	}
	vr *tmp = zm;
	vr *prev = NULL;
	while(tmp != NULL and tmp->name != a){
		prev = tmp;
		tmp = tmp->right;
	}
	if(tmp == NULL){
		tmp = new vr;
		tmp->val = b;
		tmp->name = a;
		prev->right = tmp;
		return;
	}
	tmp->val = b;
}

int get(string a){
	vr *tmp = zm;
	while(tmp->name != a) tmp = tmp->right;
	return tmp->val;
}

int main(int argc, char **argv){
	ifstream inf;
	bool fromfile = false;
	if(argc == 3){
		string path = argv[2];
		inf.open(path.c_str());
		if(inf.good())fromfile = true;
	}
	int pc;
	int progl = 0;
	comm *program = new comm;
	program->line = 0;
	bool running = true;
	int in;
	string sin;
	int regi[100];
	int acc;
	int arg;
	int cmp; // 0 equal; 1 lower; 2 greater
	while(running){
		if(fromfile) inf>>sin; else cin>>sin;
		comm *nowy = new comm;
		nowy->line = ++progl;
		nowy->left = program;
		nowy->right = NULL;
		nowy->action = sin;
		program->right = nowy;
		program = nowy;
		if(sin == "add" or sin == "sub" or sin == "mov" or sin == "mod" or sin == "cmp" or sin=="mul" or sin=="div"){
			if(fromfile){
				inf>>nowy->name1;
				inf>>nowy->name2;
			}
			else{
				cin>>nowy->name1;
				cin>>nowy->name2;
			}
		}
		else if(sin == "addn" or sin == "subn" or sin=="set" or sin == "modn" or sin=="cmpn" or sin == "muln" or sin == "divn"){
			if(fromfile){
				inf>>nowy->name1;
				inf>>nowy->arg1;
			}
			else{
				cin>>nowy->name1;
				cin>>nowy->arg1;
			}
		}
		else if(sin == "jse" or sin == "jsne" or  sin == "jmp" or sin=="je" or sin=="jne" or sin=="jl" or sin=="jle" or sin == "jg" or sin == "jge"){
			if(fromfile) inf>>nowy->name1; else cin>>nowy->name1;
		}
		else if(sin == "inc" or sin == "dec" or sin == "put" or sin == "pop" or sin == "read" or sin=="write" or sin=="amov"){
			if(fromfile) inf>>nowy->name1; else cin>>nowy->name1;
		}
		else if(sin == "end"){
			running = false;
		}
		else if(sin == "retn" or sin == "putn"){
			if(fromfile) inf>>nowy->arg1; else cin>>nowy->arg1;
		}
		else if(sin == "line"){
			if(fromfile) inf>>nowy->name1; else cin>>nowy->name1;
			set(nowy->name1,nowy->line+1);
		}
		else{
			cout<<"Syntax error! Line: "<<nowy->line<<endl;
			return 100;
		}
		
	}
	pc = 1; //Program counter
	running = true;
	while(running){
		while(program->line > pc) program = program->left;
		while(program->line < pc) program = program->right;
		pc++;
		if(program->action == "set"){
			set(program->name1,program->arg1);
		}
		else if(program->action == "write"){
			cout<<get(program->name1)<<endl;
		}
		else if(program->action == "end"){
			if(fromfile) inf.close();
			return 0;
		}
		else if(program->action == "amov")set(program->name1,acc);
		else if(program->action == "mod") acc = get(program->name1)%get(program->name2);
		else if(program->action == "modn") acc = get(program->name1)%program->arg1;
		else if(program->action == "cmp" or program->action == "cmpn"){
			int a = get(program->name1);
			int b = program->arg1;
			if(program->action == "cmp") b = get(program->name2);
			if(a==b) cmp = 0;
			if(a<b) cmp = 1;
			if(a>b) cmp = 2;
		}
		else if(program->action == "jmp") pc = get(program->name1);
		else if(program->action == "je" and cmp == 0) pc = get(program->name1);
		else if(program->action == "jne" and cmp != 0) pc = get(program->name1);
		else if(program->action == "jl" and cmp == 1) pc = get(program->name1);
		else if(program->action == "jg" and cmp == 2) pc = get(program->name1);
		else if(program->action == "jle" and (cmp == 0 or cmp == 1)) pc = get(program->name1);
		else if(program->action == "jge" and (cmp == 0 or cmp == 2)) pc = get(program->name1);
		else if(program->action == "jse" and stack==NULL) pc = get(program->name1);
		else if(program->action == "jsne" and stack!=NULL) pc = get(program->name1);
		else if(program->action == "read"){int tmp; cin>>tmp; set(program->name1,tmp);}
		else if(program->action == "mov") set(program->name1,get(program->name2));
		else if(program->action == "add") acc = get(program->name1)+get(program->name2);
		else if(program->action == "addn") acc = get(program->name1)+program->arg1;
		else if(program->action == "sub") acc = get(program->name1)-get(program->name2);
		else if(program->action == "subn") acc = get(program->name1)-program->arg1;
		else if(program->action == "mul") acc = get(program->name1)*get(program->name2);
		else if(program->action == "muln") acc = get(program->name1)*program->arg1;
		else if(program->action == "div") acc = get(program->name1)/get(program->name2);
		else if(program->action == "divn") acc = get(program->name1)/program->arg1;
		else if(program->action == "put") put(get(program->name1));
		else if(program->action == "putn") put(program->arg1);
		else if(program->action == "pop") set(program->name1,pop());
		else if(program->action == "inc") set(program->name1,get(program->name1)+1);
		else if(program->action == "dec") set(program->name1,get(program->name1)-1);
		else if(program->action == "retn") return program->arg1;
		else if(program->action == "line"){};
		
	}
}
